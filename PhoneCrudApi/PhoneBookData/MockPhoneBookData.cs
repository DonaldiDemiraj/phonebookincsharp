﻿using PhoneCrudApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhoneCrudApi.PhoneBookData
{
    public class MockPhoneBookData : IPhoneData
    {
        private List<PhoneBook> phoneBooks = new List<PhoneBook>()
        {
            new PhoneBook()
            {
                Id = Guid.NewGuid(),
                Name = "PhoneDataOne"
            },

            new PhoneBook()
            {
                Id = Guid.NewGuid(),
                Name = "PhoneDataTwo"
            }
        };
        public PhoneBook AddGetPhoneBookData(PhoneBook phoneBook)
        {
            phoneBook.Id = Guid.NewGuid();
            phoneBooks.Add(phoneBook);
            return phoneBook;
        }

        public void DeleteGetPhoneBookData(PhoneBook phoneBook)
        {
            phoneBooks.Remove(phoneBook);
        }

        public PhoneBook EditGetPhoneBookData(PhoneBook phoneBook)
        {
            var existingPhoneData = GetGetPhoneBookData(phoneBook.Id);
            existingPhoneData.Name = phoneBook.Name;
            return existingPhoneData;
        }

        public PhoneBook GetGetPhoneBookData(Guid id)
        {
            return phoneBooks.SingleOrDefault(x => x.Id == id);
        }

        public List<PhoneBook> GetPhoneBookDatas()
        {
            return phoneBooks;
        }
    }
}
