﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PhoneCrudApi.Models;

namespace PhoneCrudApi.PhoneBookData
{
    public interface IPhoneData
    {
        List<PhoneBook> GetPhoneBookDatas();

        //single data
        PhoneBook GetGetPhoneBookData(Guid id);

        //add
        PhoneBook AddGetPhoneBookData(PhoneBook phoneBook);

        //delete
        void DeleteGetPhoneBookData(PhoneBook phoneBook);

        //edit
        PhoneBook EditGetPhoneBookData(PhoneBook phoneBook);
    }
}
