﻿using PhoneCrudApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhoneCrudApi.PhoneBookData
{
    public class SqlPhoneBookData : IPhoneData
    {
        private PhoneBookContext _phoneBookContext;
        public SqlPhoneBookData(PhoneBookContext phoneBookContext)
        {
            _phoneBookContext = phoneBookContext;
        }
        public PhoneBook AddGetPhoneBookData(PhoneBook phoneBook)
        {
            phoneBook.Id = Guid.NewGuid();
            _phoneBookContext.PhoneBooks.Add(phoneBook);
            _phoneBookContext.SaveChanges();
            return phoneBook;
        }

        public void DeleteGetPhoneBookData(PhoneBook phoneBook)
        {
            _phoneBookContext.PhoneBooks.Remove(phoneBook);
            _phoneBookContext.SaveChanges();
        }

        public PhoneBook EditGetPhoneBookData(PhoneBook phoneBook)
        {
            var existingPhoneBook = _phoneBookContext.PhoneBooks.Find(phoneBook.Id);
            if(existingPhoneBook != null)
            {
                _phoneBookContext.PhoneBooks.Update(phoneBook);
                _phoneBookContext.SaveChanges();
            }
            return phoneBook;
        }

        public PhoneBook GetGetPhoneBookData(Guid id)
        { 
            var phonebook = _phoneBookContext.PhoneBooks.Find(id);
            return phonebook;
        }
            
        public List<PhoneBook> GetPhoneBookDatas()
        {
            return _phoneBookContext.PhoneBooks.ToList();
        }
    }
}
