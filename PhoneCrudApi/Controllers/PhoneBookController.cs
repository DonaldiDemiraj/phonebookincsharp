﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PhoneCrudApi.Models;
using PhoneCrudApi.PhoneBookData;

namespace PhoneCrudApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PhoneBookController : ControllerBase
    {
        private IPhoneData _phoneData;
        ///we need 5 api methods getall,getsingle,add,edit,delete
        ///
        public PhoneBookController(IPhoneData phoneData)
        {
            _phoneData = phoneData;
        }

        [HttpGet]
        [Route("api/[controller]")]
        public IActionResult GetPhoneBookDatas()
        {
            return Ok(_phoneData.GetPhoneBookDatas());
        }

        [HttpGet]
        [Route("api/[controller]/{id}")]
        public IActionResult GetGetPhoneBookData(Guid id)
        {
            var phonedata = _phoneData.GetGetPhoneBookData(id);

            if(phonedata != null)
            {
                return Ok(phonedata);
            }

            return NotFound($"PhoneData with Id: {id} was not found");
            
        }

        [HttpPost]
        [Route("api/[controller]")]
        public IActionResult AddGetPhoneBookData(PhoneBook phoneBook)
        {
             _phoneData.AddGetPhoneBookData(phoneBook);

            return Created(HttpContext.Request.Scheme + "://" + HttpContext.Request.Host + HttpContext.Request.Path + "/" + phoneBook.Id,
                phoneBook);

        }

        [HttpDelete]
        [Route("api/[controller]/{id}")]
        public IActionResult DeleteGetPhoneBookData(Guid id)
        {
            var phonedata = _phoneData.GetGetPhoneBookData(id);

            if(phonedata != null)
            {
                _phoneData.DeleteGetPhoneBookData(phonedata);
                return Ok();
            }

            return NotFound($"PhoneData with Id: {id} was not found");
        }

        [HttpPatch]
        [Route("api/[controller]/{id}")]
        public IActionResult EditGetPhoneBookData(Guid id, PhoneBook phoneBook)
        {
            //if exist or not
            var existingPhoneData = _phoneData.GetGetPhoneBookData(id);

            if (existingPhoneData != null)
            {
                phoneBook.Id = existingPhoneData.Id;
                _phoneData.EditGetPhoneBookData(phoneBook);
            }

            return Ok(phoneBook);

        }


    }
}
